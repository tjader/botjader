#!/usr/bin/env python3

import asyncio
import nio
import sqlite3
import datetime
import functools
import random
import math
import unicodedata
import re

bot_commands = {}

database = sqlite3.connect('botjader.sqlite')

async def user_display_name(user, room=None):
	display = room.user_name(user)

	if not display:
		display = (await client.get_displayname(user)).displayname

	if not display:
		display = user

	return display

def noping(s):
	try:
		return '\u200C'.join(s)
	except TypeError:
		return ''

def bot_command(command_names, doc=None):
	def f(func):
		for command_name in command_names:
			print(f'adicionando {command_name} -> {func}')
			bot_commands[command_name] = (func, doc)
		return func
	return f

def club_init():
	for club, short in database.execute('SELECT club, short FROM clubs'):
		bot_commands[short] = (functools.partial(club_short, club), None)
		bot_commands['des' + short] = (functools.partial(desclub_short, club), None)


@bot_command(['club'])
async def club(room, event):
	try:
		_, command, params = event.body.split(maxsplit=2)
	except ValueError:
		return

	if event.sender == '@irc_snoonet_tjader:tjader.xyz':
		if command == 'remove':
			club, user = params.split(maxsplit=1)

			user = f'@irc_snoonet_{user}:tjader.xyz'
			print(room.machine_name, club, user)
			database.execute('DELETE FROM club_status WHERE room = ? AND club = ? AND user = ?', (room.machine_name, club, user))
			database.commit()


	if command == 'leave':
		club = params

		club_leave(room.machine_name, club, event.sender)

	elif command == 'status':
		try:
			club, status = params.split(maxsplit=1)
		except ValueError:
			return

		club_status(room.machine_name, club, event.sender, status)

	elif command == 'show':
		club = params

		await club_show(room, club)


def club_status(room, club, user, status):
	database.execute('REPLACE INTO club_status (room, club, user, status, time) VALUES (?, ?, ?, ?, ?)', (room, club, user, status, datetime.datetime.now()))
	database.commit()

def club_leave(room, club, user):
	database.execute('DELETE FROM club_status WHERE room = ? AND club = ? AND user = ?', (room, club, user))
	database.commit()

async def club_show(room, club):
	users_status = []
	for user, status in database.execute('SELECT user, status FROM club_status WHERE room = ? AND club = ?', (room.machine_name, club)):
		user = noping(await user_display_name(user, room))
		users_status.append(f'{user}: {status}')

	await client.room_send(
		room_id=room.room_id,
		message_type='m.room.message',
		content = {
			'msgtype': 'm.text',
			'body': f'{", ".join(users_status)}'
		}
	)

async def club_short(club, room, event):
	status = None
	try:
		status = event.body.split(maxsplit=1)[1]
	except IndexError:
		pass

	if status:
		club_status(room.machine_name, club, event.sender, status)
	else:
		await club_show(room, club)

async def desclub_short(club, room, event):
	club_leave(room.machine_name, club, event.sender)

def zalgo(n):
	return '\u034f'.join(random.choices(zalgo.marks, k=n))

zalgo.marks = tuple(chr(point) for point in range(0x300, 0x370) if point != 0x34f)

@bot_command(['zalgo', 'z'])
async def zalguifica(room, event):
	s = unicodedata.normalize('NFD', ' '.join(event.body.split()[1:]))

	if re.fullmatch('[^a-z]*ai[^a-z]*ai[^a-z]*', unicodedata.normalize('NFKD', ' '.join(event.body.split()[1:])), re.IGNORECASE):
		s = f'bã no {await user_display_name(event.sender, room)}'

	s = list(s)

	n = int(math.log(random.random(), 1 - 0.25)) + 1
	body = ''
	while len(s) > 0:
		c = s.pop(0)
		body += c
		if unicodedata.category(c)[0] in 'LNS':
			while len(s) > 0 and unicodedata.category(s[0])[0] == 'M':
				body += s.pop(0)
			body += zalgo(n)

	body = unicodedata.normalize('NFC', body)

	await client.room_send(
		room_id=room.room_id,
		message_type='m.room.message',
		content = {
			'msgtype': 'm.text',
			'body': body
		}
	)

@bot_command(['aiai'])
async def aiai(room, event):
	n = int(math.log(random.random(), 1 - 0.5)) + 1
	z = zalgo(n)

	aiai = unicodedata.normalize('NFC', f'A{z}i a{z[:math.ceil(len(z)/2)]}i!')

	await client.room_send(
		room_id=room.room_id,
		message_type='m.room.message',
		content = {
			'msgtype': 'm.text',
			'body': aiai
		}
	)


async def message_cb(room, event):
	if room.machine_name not in config['enabled_rooms']:
		return

	if event.body and event.body[0] == '!':
		command = event.body.split(maxsplit=1)[0][1:]

		if command in bot_commands:
			print(command, bot_commands[command])
			await bot_commands[command][0](room, event)

config = {}
for name, value in database.execute('SELECT name, value FROM config'):
	config[name] = value
config['enabled_rooms'] = set(config['enabled_rooms'].split(','))

club_init()

client = nio.AsyncClient(config['homeserver'], config['user'])
client.add_event_callback(message_cb, nio.RoomMessageText)
client.access_token = config['access_token']

async def main():
	await client.sync_forever(timeout=30000, first_sync_filter={'room':{'timeline':{'limit':0}}})

asyncio.get_event_loop().run_until_complete(main())
